package br.com.devrybrasil.metrocamp.distribuidos;

public class InvalidParameters extends Exception {
    public InvalidParameters() { super(); }
    public InvalidParameters(String message) { super(message); }
    public InvalidParameters(String message, Throwable cause) { super(message, cause); }
}
