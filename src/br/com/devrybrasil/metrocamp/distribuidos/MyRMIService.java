package br.com.devrybrasil.metrocamp.distribuidos;

import java.util.List;

public interface MyRMIService {
    public String getMessage();
    public List<Integer> getMultiples(int limit, int step) throws InvalidParameters;
    public List<Character> asCharList(String string);
}

