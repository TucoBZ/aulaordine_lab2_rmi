package br.com.devrybrasil.metrocamp.distribuidos;

public class NamingCodes {
    public static final int ADD = 0;
    public static final int REMOVE = 1;
    public static final int FIND = 2;
}
