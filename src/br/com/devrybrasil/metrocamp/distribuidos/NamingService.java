package br.com.devrybrasil.metrocamp.distribuidos;

public interface NamingService {
    public void add(String name, Address address);
    public void remove(String name);
    public Address find(String name);
}