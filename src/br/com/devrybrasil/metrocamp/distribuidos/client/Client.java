package br.com.devrybrasil.metrocamp.distribuidos.client;

import br.com.devrybrasil.metrocamp.distribuidos.InvalidParameters;
import br.com.devrybrasil.metrocamp.distribuidos.MyRMIService;

public class Client {
    public static void main(String[] args) throws InvalidParameters {

        MyRMIService stub = new Stub();


        try {
            //Get Message
            System.out.println(stub.getMessage());

            //Get Multiples
            System.out.println(stub.getMultiples(50,10));
            System.out.println(stub.getMultiples(45,3));

            //As Char List
            System.out.println(stub.asCharList("Qual é a lista de string dessa frase?"));

            //Get Multiples - Erro teste
            System.out.println(stub.getMultiples(0,10));

            //As Char List - Erro teste
            System.out.println(stub.asCharList(null));
        } catch (InvalidParameters e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
            System.out.println(e);
        }

    }
}
