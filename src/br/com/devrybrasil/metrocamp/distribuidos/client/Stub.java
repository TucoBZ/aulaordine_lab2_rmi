package br.com.devrybrasil.metrocamp.distribuidos.client;

import br.com.devrybrasil.metrocamp.distribuidos.*;
import br.com.devrybrasil.metrocamp.distribuidos.naming.client.NamingStub;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class Stub implements MyRMIService {

    @Override
    public String getMessage() {

        String returnValue = null;

        try {

            //Lê String
            returnValue = (String)this.readObject(null, RMICodes.GETMESSAGE);

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();

        }

        return returnValue;
    }

    @Override
    public List<Integer> getMultiples(int limit, int step) {
        List<Integer> returnValue = null;

        try {

            //Escreve parâmetros
            List<Integer> parameters = new ArrayList<Integer>();
            parameters.add(limit);
            parameters.add(step);

            //Lê String
            returnValue = (List<Integer>)this.readObject(parameters, RMICodes.GETMULTIPLES);

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
        }

        return returnValue;

    }

    @Override
    public List<Character> asCharList(String string) {
        List<Character> returnValue = null;

        try {

            //Escreve parâmetros
            List<String> parameters = new ArrayList<String>();
            parameters.add(string);

            //Lê String
            returnValue = (List<Character>)this.readObject(parameters, RMICodes.GETASCHARLIST);

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
        }

        return returnValue;
    }


    public final Object readObject(Object parameters, Integer code) throws ClassNotFoundException {
        Object returnValue = null;
        Socket socket = null;

        try {

            //Obtém endereço do Servidor
            NamingService stub = new NamingStub ();
            Address rmiAddress = stub.find("RMI_SERVER");

            //Cria conexão
            socket = new Socket(rmiAddress.getHost(), rmiAddress.getPort());

            //Cria canais de comunicação
            DataInputStream in = new DataInputStream(socket.getInputStream());
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeInt(code);

            //Canal de escrita de objetos
            ObjectOutputStream output = new ObjectOutputStream(out);

            //Escreve parâmetros
            if (parameters != null) {
                output.writeObject(parameters);
            }


            //Canal de leitura de objetos
            ObjectInputStream input = new ObjectInputStream(in);

            //Lê String
            returnValue = input.readObject();

        } catch (IOException e) {
            //Deveriamos tratar o erro
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();

        } finally {
            try {
                //Fecha conexão, se possível
                socket.close();
            } catch (Exception e) {
                //Deveriamos tratar o erro
                e.printStackTrace();
            }
        }

        return returnValue;
    }
}
