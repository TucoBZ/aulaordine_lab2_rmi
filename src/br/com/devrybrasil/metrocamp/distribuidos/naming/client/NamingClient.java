package br.com.devrybrasil.metrocamp.distribuidos.naming.client;

import br.com.devrybrasil.metrocamp.distribuidos.Address;
import br.com.devrybrasil.metrocamp.distribuidos.InvalidParameters;
import br.com.devrybrasil.metrocamp.distribuidos.NamingService;

public class NamingClient {
    public static void main(String[] args) throws InvalidParameters {

        NamingService stub = new NamingStub ();

        Address address1 = new Address("localhost", 1234);
        Address address2 = new Address("localhost", 1235);

        System.out.println("Adicionando Endereços");
        stub.add ("RmiAddress", address1);
        stub.add ("NamingAddress", address2);

        System.out.println("Verificando se acha os endereços Endereços");
        System.out.println(stub.find ("RmiAddress"));
        System.out.println(stub.find ("NamingAddress"));

        System.out.println("Removendo Endereço: RmiAddress");
        stub.remove ("RmiAddress");
        System.out.println(stub.find ("RmiAddress"));
        System.out.println(stub.find ("NamingAddress"));

        System.out.println("Removendo Endereço: NamingAddress");
        stub.remove ("NamingAddress");
        System.out.println(stub.find ("RmiAddress"));
        System.out.println(stub.find ("NamingAddress"));

    }
}
