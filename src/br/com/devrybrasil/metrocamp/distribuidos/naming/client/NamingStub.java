package br.com.devrybrasil.metrocamp.distribuidos.naming.client;

import br.com.devrybrasil.metrocamp.distribuidos.Address;
import br.com.devrybrasil.metrocamp.distribuidos.NamingCodes;
import br.com.devrybrasil.metrocamp.distribuidos.NamingService;
import br.com.devrybrasil.metrocamp.distribuidos.client.Configuration;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class NamingStub implements NamingService {
    @Override
    public void add(String name, Address address) {
        try {

            //Escreve parâmetros
            ArrayList<Object> parameters = new ArrayList<Object>();
            parameters.add(name);
            parameters.add(address);

            //Lê String
            this.readObject(parameters, NamingCodes.ADD);

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
        }

    }

    @Override
    public void remove(String name) {
        try {

            //Escreve parâmetros
            List<String> parameters = new ArrayList<String>();
            parameters.add(name);

            //Lê String
            this.readObject(parameters, NamingCodes.REMOVE);

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
        }

    }

    @Override
    public Address find(String name) {
        Address returnValue = null;

        try {

            //Escreve parâmetros
            List<String> parameters = new ArrayList<String>();
            parameters.add(name);

            //Lê String
            returnValue = (Address) this.readObject(parameters, NamingCodes.FIND);

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
        }

        return returnValue;
    }

    public final Object readObject(Object parameters, Integer code) throws ClassNotFoundException {
        Object returnValue = null;
        Socket socket = null;

        try {

            //Obtém endereço do Servidor
            Address rmiAddress = Configuration.getNamingServerAddress();

            //Cria conexão
            socket = new Socket(rmiAddress.getHost(), rmiAddress.getPort());

            //Cria canais de comunicação
            DataInputStream in = new DataInputStream(socket.getInputStream());
            DataOutputStream out = new DataOutputStream(socket.getOutputStream());
            out.writeInt(code);

            //Canal de escrita de objetos
            ObjectOutputStream output = new ObjectOutputStream(out);

            //Escreve parâmetros
            if (parameters != null) {
                output.writeObject(parameters);
            }


            //Canal de leitura de objetos
            ObjectInputStream input = new ObjectInputStream(in);

            //Lê String
            returnValue = input.readObject();

        } catch (IOException e) {
            //Deveriamos tratar o erro
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();

        } finally {
            try {
                //Fecha conexão, se possível
                socket.close();
            } catch (Exception e) {
                //Deveriamos tratar o erro
                e.printStackTrace();
            }
        }

        return returnValue;
    }
}
