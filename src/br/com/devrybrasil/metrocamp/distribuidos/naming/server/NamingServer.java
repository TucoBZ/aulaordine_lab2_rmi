package br.com.devrybrasil.metrocamp.distribuidos.naming.server;


public class NamingServer {
    public static void main(String[] args) {

        NamingMultiThreadServer server = new NamingMultiThreadServer(1236);

        server.startServer();
    }
}
