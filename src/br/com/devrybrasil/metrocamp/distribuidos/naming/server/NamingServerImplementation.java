package br.com.devrybrasil.metrocamp.distribuidos.naming.server;

import br.com.devrybrasil.metrocamp.distribuidos.Address;
import br.com.devrybrasil.metrocamp.distribuidos.NamingService;

import java.util.HashMap;
import java.util.Map;

public class NamingServerImplementation implements NamingService {

    private static NamingServerImplementation singleton = new NamingServerImplementation();

    /* A private Constructor prevents any other
    * class from instantiating.
    */
    private NamingServerImplementation() { }

    /* Static 'instance' method */
    public static NamingServerImplementation getInstance( ) {
        return singleton;
    }

    Map<String,Address> addressMap = new HashMap<String,Address>();

    @Override
    synchronized public void add(String name, Address address) {
        System.out.println("Add");
        System.out.println(addressMap);
        System.out.println(name);
        System.out.println(address);
        addressMap.put(name,address);
        System.out.println(addressMap);
        System.out.println("Add Finish");
    }

    @Override
    synchronized public void remove(String name) {
        System.out.println("Remove");
        System.out.println(addressMap);
        System.out.println(name);
        addressMap.remove(name);
        System.out.println(addressMap);
        System.out.println("Remove Finish");
    }

    @Override
    synchronized public Address find(String name) {
        System.out.println("Find");
        System.out.println(addressMap);
        System.out.println(name);
        System.out.println(addressMap.get(name));
        System.out.println("Find Finish");
        return addressMap.get(name);

    }

}