package br.com.devrybrasil.metrocamp.distribuidos.naming.server;

import br.com.devrybrasil.metrocamp.distribuidos.Address;
import br.com.devrybrasil.metrocamp.distribuidos.InvalidParameters;
import br.com.devrybrasil.metrocamp.distribuidos.NamingCodes;
import br.com.devrybrasil.metrocamp.distribuidos.NamingService;


import java.io.*;
import java.net.Socket;
import java.util.List;

public class NamingSkeleton implements Runnable {
    Socket socketConnection;
    NamingService namingService = NamingServerImplementation.getInstance();

    NamingSkeleton(Socket socket) {
        this.socketConnection = socket;
    }

    @Override
    public void run() {
        try {
            //Cria canais de comunicação
            DataInputStream in = new DataInputStream(socketConnection.getInputStream());
            DataOutputStream out = new DataOutputStream(socketConnection.getOutputStream());

            //Obtém o método que deve ser tratado
            int methodCalled = in.readInt();

            switch (methodCalled) {
                case NamingCodes.ADD:
                    handleAddNameAddress(in,out);
                    break;
                case NamingCodes.REMOVE:
                    handleRemoveNameAddress(in,out);
                    break;
                case NamingCodes.FIND:
                    handleFindNameAddress(in,out);
                    break;
                default:
                    break;
            }

            //Fecha conexão
            socketConnection.close();

        }catch (IOException e) {
            //Deveriamos tratar o erro
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
        } catch (InvalidParameters e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
            System.out.println(e);
        }
    }

    private void handleAddNameAddress(DataInputStream in, DataOutputStream out) throws IOException, ClassNotFoundException, InvalidParameters {
        //Obtém lista de parâmetros
        ObjectInputStream input = new ObjectInputStream(in);
        List<Object> parameters= (List<Object>)input.readObject();

        if (parameters.size() != 2) {
            throw new InvalidParameters("Invalid Parameter Size");
        }

        //Executa método remoto
        String name = (String)parameters.get(0);
        Address address = (Address)parameters.get(1);

        if (name == null || address == null) {
            throw new InvalidParameters("Invalid Parameters Types");
        }

        namingService.add(name,address);

        //Canais de envio de objetos
        ObjectOutputStream output = new ObjectOutputStream(out);

        //Escreve valor de retorno
        output.writeObject(true);
    }

    private void handleRemoveNameAddress(DataInputStream in, DataOutputStream out) throws IOException, ClassNotFoundException, InvalidParameters {
        //Obtém lista de parâmetros
        ObjectInputStream input = new ObjectInputStream(in);
        List<String> parameters= (List<String>)input.readObject();

        if (parameters.size() != 1) {
            throw new InvalidParameters("Invalid Parameter Size");
        }

        //Chama o método remoto
        namingService.remove(parameters.get(0)); ;

        //Envia resposta
        ObjectOutputStream output = new ObjectOutputStream(out);
        output.writeObject(true);
    }

    private void handleFindNameAddress(DataInputStream in, DataOutputStream out) throws IOException, ClassNotFoundException, InvalidParameters {
        //Obtém lista de parâmetros
        ObjectInputStream input = new ObjectInputStream(in);
        List<String> parameters= (List<String>)input.readObject();

        if (parameters.size() != 1) {
            throw new InvalidParameters("Invalid Parameter Size");
        }

        //Chama o método remoto
        Address result = namingService.find(parameters.get(0));

        //Envia resposta
        ObjectOutputStream output = new ObjectOutputStream(out);
        output.writeObject(result);
    }
}
