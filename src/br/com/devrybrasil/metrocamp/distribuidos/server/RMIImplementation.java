package br.com.devrybrasil.metrocamp.distribuidos.server;


import br.com.devrybrasil.metrocamp.distribuidos.InvalidParameters;
import br.com.devrybrasil.metrocamp.distribuidos.MyRMIService;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class RMIImplementation implements MyRMIService {

    @Override
    public String getMessage() {
        return "String do servidor, agora no lugar certo!";
    }

    @Override
    public List<Integer> getMultiples(int limit, int step) throws InvalidParameters {

        if (limit < 1) {
            throw new InvalidParameters("Invalid Limit");
        }

        List<Integer> multiples = new ArrayList<Integer>();

        for (int i = 0; i < limit; i++) {
            if (i % step == 0) {
                multiples.add(i);
            }
        }

        return  multiples;
    }

    @Override
    public List<Character> asCharList(String string) {
        ArrayList<Character> chars = new ArrayList<Character>();
        for (char c : string.toCharArray()) {
            chars.add(c);
        }
        return chars;
    }
}

