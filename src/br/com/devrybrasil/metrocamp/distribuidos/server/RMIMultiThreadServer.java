package br.com.devrybrasil.metrocamp.distribuidos.server;


import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class RMIMultiThreadServer {

    private int port;

    RMIMultiThreadServer(int port) {
        this.port = port;
    }

    public void startServer() {
        try {
            ServerSocket ssock = new ServerSocket(port);

            //Loop infinito
            while (true) {

                //Espera conexões
                Socket socket = ssock.accept();
                System.out.println("Nova conexão");

                //Cria uma thread para cuidar da conexão
                Skeleton workServer = new Skeleton(socket);
                Thread workThread = new Thread(workServer);
                workThread.start();
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
