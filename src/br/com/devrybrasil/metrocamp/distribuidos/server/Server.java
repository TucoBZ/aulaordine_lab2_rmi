package br.com.devrybrasil.metrocamp.distribuidos.server;

import br.com.devrybrasil.metrocamp.distribuidos.Address;
import br.com.devrybrasil.metrocamp.distribuidos.NamingService;
import br.com.devrybrasil.metrocamp.distribuidos.client.Configuration;
import br.com.devrybrasil.metrocamp.distribuidos.naming.client.NamingStub;

public class Server {

    public static void main(String[] args) {

        Address address = Configuration.getRMIServerAddress();

        RMIMultiThreadServer server = new RMIMultiThreadServer(address.getPort());

        NamingService stub = new NamingStub ();
        stub.add ("RMI_SERVER", address);

        server.startServer();
    }
}

