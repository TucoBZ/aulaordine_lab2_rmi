package br.com.devrybrasil.metrocamp.distribuidos.server;

import br.com.devrybrasil.metrocamp.distribuidos.InvalidParameters;
import br.com.devrybrasil.metrocamp.distribuidos.MyRMIService;
import br.com.devrybrasil.metrocamp.distribuidos.RMICodes;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class Skeleton implements Runnable {

    Socket socketConnection;
    MyRMIService rmiService = new RMIImplementation();

    Skeleton(Socket socket) {
        this.socketConnection = socket;
    }

    @Override
    public void run() {
        try {
            //Cria canais de comunicação
            DataInputStream in = new DataInputStream(socketConnection.getInputStream());
            DataOutputStream out = new DataOutputStream(socketConnection.getOutputStream());
            System.out.println(in);
            //Obtém o método que deve ser tratado
            int methodCalled = in.readInt();

            switch (methodCalled) {
                case RMICodes.GETMESSAGE:
                    handleGetMessage(in,out);
                    break;
                case RMICodes.GETMULTIPLES:
                    handleGetMultiples(in,out);
                    break;
                case RMICodes.GETASCHARLIST:
                    handleAsCharList(in,out);
                    break;
                default:
                    break;
            }

            //Fecha conexão
            socketConnection.close();

        }catch (IOException e) {
            //Deveriamos tratar o erro
            e.printStackTrace();

        } catch (ClassNotFoundException e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
        } catch (InvalidParameters e) {
            // Nova exceção tratada!!!
            e.printStackTrace();
            System.out.println(e);
        }
    }

    private void handleGetMessage(DataInputStream in, DataOutputStream out) throws IOException {
        //Executa método remoto

        String result = rmiService.getMessage();

        //Canais de envio de objetos
        ObjectOutputStream output = new ObjectOutputStream(out);

        //Escreve valor de retorno
        output.writeObject(result);
    }

    private void handleGetMultiples(DataInputStream in, DataOutputStream out) throws IOException, ClassNotFoundException, InvalidParameters {
        //Obtém lista de parâmetros
        ObjectInputStream input = new ObjectInputStream(in);
        List<Integer> parameters= (List<Integer>)input.readObject();

        if (parameters.size() != 2) {
            throw new InvalidParameters("Invalid Parameter Size");
        }

        //Chama o método remoto
        List<Integer> result = rmiService.getMultiples(parameters.get(0), parameters.get(1));

        //Envia resposta
        ObjectOutputStream output = new ObjectOutputStream(out);
        output.writeObject(result);
    }

    private void handleAsCharList(DataInputStream in, DataOutputStream out) throws IOException, ClassNotFoundException, InvalidParameters {
        //Obtém lista de parâmetros
        ObjectInputStream input = new ObjectInputStream(in);
        List<String> parameters= (List<String>)input.readObject();

        if (parameters.size() != 1) {
            throw new InvalidParameters("Invalid Parameter Size");
        }

        //Chama o método remoto
        List<Character> result = rmiService.asCharList(parameters.get(0));

        //Envia resposta
        ObjectOutputStream output = new ObjectOutputStream(out);
        output.writeObject(result);
    }
}
